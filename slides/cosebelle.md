
## Liquidsoap, cose belle

---

## Supporto alla galassia multimediale

* __Codec interni__: opus, vorbis, mpeg-3, mpeg-4, theora
* __Codec esterni__: encoder e decoder
* __I/O__: Alsa, portaudio, jack, v4l2
* __Output__: file, HTTP, icecast, HLS, SRT
* __Input__: Harbor (icecast) input ffmpeg, gstreamer
* __Input protocol__: Youtube (via RTMP & ffmpeg), beets, youtube-dl, mpd,
  process, s3, say (text-to-speech), synth, text-to-wave, etc...

---

## Manipolazione audio/video

* __Audio__: normalization, amplify, replaygain, cross-fading, rilevamento
  rumore pianco, mono2stereo, stereo2mono, etc...
* __Audio/Video__: Ladspa, dssi, lilv & ffmpeg filtri ffmpeg

---

### Il linguaggio di script

* threading
* configurazioni dinamiche
* input api (telnet, http)
* self-documented
* fortemente tipato
* clock integrati

