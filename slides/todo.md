
### Cose rimaste

- rilevamento silenzio
- output metadati

---

### Progetti carini

- [__AzuraCast__](https://www.azuracast.com/)
- [__AuRa__](https://gitlab.servus.at/aura): Automated Radio
- [__AutoRadio__](https://git.autistici.org/ale/autoradio): tante grazie a [streampunk.cc](streampunk.cc/)
- [__Radio france__](https://www.radiofrance.com/): eh già

---

### Presentazioni

- [Functional audio and video stream generation with Liquidsoap](https://archive.fosdem.org/2020/schedule/event/om_liquidsoap/)
- [Streaming at Radio France](https://www.youtube.com/watch?v=UnHfgDmi9_w&t=1s)
- [Easing automation and improving your sound with Liquidsoap and FFmpeg](https://www.youtube.com/watch?v=FPPATbOZKgg)
- [Liquidsoap current and future features](https://www.youtube.com/watch?v=VT6TEjJzWoY&feature=emb_title)
