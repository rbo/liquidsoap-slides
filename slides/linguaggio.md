## Liquidsoap, il linguaggio

---

```liquidsoap[|1-2|4|5-8]
myplaylist = playlist("~/radio/music.m3u")
jingles = playlist("~/radio/jingles.m3u")
radio = myplaylist
radio = random(weights = [1, 4],[jingles, radio])
output.icecast(%mp3,
	host = "localhost", port = 8000,
	password = "hackme", mount = "basic-radio",
	radio)
```

```console
At line 5, char 8-49:
Error 7: Invalid value:
That source is fallible
```
<!-- .element: class="fragment" -->

---

```liquidsoap[3|6]
myplaylist = playlist("~/radio/music.m3u")
jingles = playlist("~/radio/jingles.m3u")
security = single("~/radio/sounds/default.mp3")
radio = myplaylist
radio = random(weights = [1, 4],[jingles, radio])
radio = fallback(track_sensitive = false, [radio, security])
output.icecast(%mp3,
	host = "localhost", port = 8000,
	password = "hackme", mount = "basic-radio",
	radio)
```

```console
At line 7, char 8-49:
Error 7: Invalid value:
That source is fallible
```
<!-- .element: class="fragment" -->

---

```liquidsoap[7]
myplaylist = playlist("~/radio/music.m3u")
jingles = playlist("~/radio/jingles.m3u")
security = single("~/radio/sounds/default.mp3")
radio = myplaylist
radio = random(weights = [1, 4],[jingles, radio])
radio = fallback(track_sensitive = false, [radio, security])
radio = mksafe(radio)
output.icecast(%mp3,
	host = "localhost", port = 8000,
	password = "hackme", mount = "basic-radio",
	radio)
```

---

```liquidsoap[6-8]
myplaylist = playlist("~/radio/music.m3u")
jingles = playlist("~/radio/jingles.m3u")
security = single("~/radio/sounds/default.mp3")
radio = myplaylist
radio = random(weights = [1, 4],[jingles, radio])
radio = switch(
		track_sensitive = true,
		[ ({ 4w and 13h-15h }, radio) ])
radio = fallback(track_sensitive = false, [radio, security])
radio = mksafe(radio)
output.icecast(%mp3,
	host = "localhost", port = 8000,
	password = "hackme", mount = "basic-radio",
	radio)

```

---

```liquidsoap[10]
myplaylist = playlist("~/radio/music.m3u")
jingles = playlist("~/radio/jingles.m3u")
security = single("~/radio/sounds/default.mp3")
radio = myplaylist
radio = random(weights = [1, 4],[jingles, radio])
radio = switch(
		track_sensitive = true,
		[ ({ 4w and 13h-15h }, radio) ])
radio = fallback(track_sensitive = false, [radio, security])
radio = crossfade(fade_out=3., fade_in=3., duration=5., radio)
radio = mksafe(radio)
output.icecast(%mp3,
	host = "localhost", port = 8000,
	password = "hackme", mount = "basic-radio",
	radio)

```
