## Liquidsoap, la planetaria multimediale

> a.k.a Come non imparare le flag di ffmpeg e vivere felici

---

### Storia

* Iniziato da __David Barelde__ e __Samuel Mimram__ come progetto
  scolastico a Lione nel 2004, chiaramente la prima versione non funziona.
* Inizialmente era uno script in perl che interagiva con __Ices__.
* Successivamente si aggiungono Ramain Beauxis ed il progetto si spota da
  Lione a Parigi. Da __savonet__ nasce __liquidsoap__.

---

### La fase OCaml

* __Savonet__: _SAm and daVid Ocaml NETwork_
* Da maledetti studenti universitari hanno voluto fare la roba funzionale.
* A peggiorare la faccenda __OCaml__ in quel periodo era il linguaggio più di
  moda in Francia (w l'autarchia dei linguaggi).
* Così nasce la loro prima web radio: _geekradio_.

