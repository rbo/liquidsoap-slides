## Bobina

Fatta con ❤️ dal black tech team di Radio Blackout 105.250FM

---

![blackout](img/blackout.svg)

---

![bobina](img/bobina.svg)

---

## I flussi

L'elenco è in ordine decrescente di priorità, per il tipo di flussi che
gestiamo il passaggio da un flusso ad un altro dovrebbe essere istantaneo e
non legato alla fine di una traccia dato che molti flussi potrebbero non
finire mai (tracksensitive=false).

---

### Live

- __mobile__: occhio ai buffer e deve essere esposto all'interweb
- __blackout house__: rete interna, pochi problemi
- __trasmissione__ (es radio notav): come mobile, ma ogni trasmissione ha
  degli accessi diversi e limitata nell'orario. Se provi a trasmettere
  fuori dal tuo orario arriva un http unauthorized. Sarebbe bello dare alle
  trasmissioni un endpoint di debug/gioco.

---

### Trasmissioni

- __programmata__: un file audio che deve essere riprodotto per intero ad
  un determinato orario, viene esplicitamente inserita.
- __differita__: un file audio che deve essere riprodotto per intero,
  viene scelta casualmente da un ventaglio di papabili.

---

### Archivio

- __playlist__: una playlist di pezzi selezionati, deve essere riprodotta
  per intero
- __jingle__: andrebbero pesati in base alle date, attualmente è rarissimo
  sentirne uno
- __musica__: singoli pezzi selezionati a casaccio (attualmente
  bobina_rotazione.liq)

---

## Come lo facciamo?

- teniamo la logica dentro liquidsoap?
- interagiamo con liquidsoap dicendogli esegui questo o altro?
- riusciremo ad integrare le differite in modo sensato?
- ma c'è un modo di fare delle selezioni musicali meglio di rotazione?
- liquidsoap è potente, ma un po' fastidioso da scrivere.

---

## Proviamoci con liquidsoap

Liquidsoap ha la possibilità di essere "comandato" da fuori con alcune
interfacce (telnet, http, variabili interattive, etc), il problema è che
mentre è estremamente comodo gestire i flussi (es: se quell'input è spento,
usa altro) ed in generale tutto quello che è manipolazione multimediale non
si può dire lo stesso di scrivere la parte logica (es: programma questa
trasmissione). Insomma giustamente liquidsoap è un linguaggio creato con un
obiettivo in testa e non per fare tutto.

---

Fatte queste premesse teniamo liquidsoap per fare le seguenti cose:

- gestisci flussi con priorità
- raccogli flussi esterni con eventuali permessi
- logga (la nostra unica fonte di verità)
- deve essere in grado di funzionare anche quando gli altri componenti
  muoiono o sono irraggiungibili. Nella pratica si traduce nell'avere
  sempre un fallback sensato (non vale rumore bianco).

---

Cosa delega?

- logica: se non ho live... cosa riproduco? me lo dice l'oracolo!
- interfaccia: santo cielo javascript...

---

## L'oracolo DJ

Risponde alla domanda: cosa riproduco? Cosa usa per rispondere? (in ordine
di priorità)

- C'è una trasmissione programmata?
- Siamo in un buco del palinsesto? Cerco una differita che stia nel tempo e
  nel tipo richiesto
- Ho delle playlist musicali che mi coprono il tempo fino alla prossima
  trasmissione?
- Scelgo un pezzo a caso chiedendolo a beets

---

Quali informazioni servono?

- palinsesto
- calendario buchi
- trasmissioni programmate
- playlist

---

Deve avere una serie di interfacce che potranno essere ricavate dal nuovo
sito o da robe dedicate come l'automatico:

- programmate: l'automatico
- playlist: creare playlist a partire dall'archivio
- musica: mpd, beet: è necessario un server/archivio indicizzato, basta
  cartelle condivise o meglio... teniamole in sola lettura e facciamo
  caricare la musica in altro modo.
- calendario buchi: ehm giovedì non trasmetto dalle 13 alle 15, ci starebbe
  un contenuto informativo!

---

## E ora, mani in pasta
